#include <vector>
#include <unistd.h>
#include <time.h>

#include "GLProgram.h"
#include "Mesh.h"
#include <glm/glm.hpp>
#include <glm/gtc/random.hpp>
#include "Player.h"
#include "Camera.h"
#include "Controles.h"
#include "Clock.h"

#include "Scene.h"
#include "PhongRenderer.h"
#include "PhongShaderProxy.h"
#include "Cube.h"
#include "Material.h"
#include "Portal.h"
#include "PlayerCamera.h"
#include "PortalCamera.h"
#include "PortalRenderer.h"
#include "CurserRenderer.h"
#include "fmod/inc/fmod.hpp"
#include "fmod/inc/fmod_errors.h"


/*
void updateFPSCounter(float dt){
	static char fps[255];
	static float nFrames = 0;
	static float t = 0.0f;
	nFrames++;
	t += dt;
	if( t >= 1.0) {
		printf("FPS: %f fps", nFrames/t);
		nFrames = 0.0f;
		t = 0;
	}
}*/

int main( int argc, char** argv ){
	srand (time(NULL));
	printf("enter program\n");

	GLProgram glProgram("Game Dev final project");
	printf("configured program\n");

	Clock clock;

	PlayerPropeties playerProperties;
	playerProperties.speed = 20; // units per second
	playerProperties.jumpSpeed = 30;
	playerProperties.height = 3;
	playerProperties.angleSpeed = 0.1;

	glm::mat4 projection = perspective(45.0f,(float)glProgram.getWinWidth() / glProgram.getWinHeight(), 0.1f, 500.0f);
	printf("screen :(%f. %f)\n", (float)glProgram.getWinWidth(), (float)glProgram.getWinHeight());

	Material outerMaterial;
	outerMaterial.color = vec3(0.9,0.9,0.9);
	outerMaterial.kDiffuse = 0.4;

	Portal portal1(vec3(-50,20,20), vec2(5,5), vec3(0,0,-1));
	Portal portal2(vec3(40,10,-40), vec2(5,5), vec3(0,0,1));
	//Portal portal3(vec3(40,10,-40), vec2(5,5), vec3(0,0,1));

	PhongRenderer phongRenderer(glProgram.getContext());

	Scene scene;
	scene.setProjection(projection);

	Cube outer(vec3(0,100,0), vec3(100,100,100), outerMaterial, false);
	scene.addEntity(&outer, & phongRenderer);

	Material cubeMaterial;
	cubeMaterial.color = vec3(0.4,0.4,0.7);
	cubeMaterial.kDiffuse = 0.7;

	int cubeGridSize = 3;

	for(int i=0; i < cubeGridSize ; i++){
		for(int j=0; j < cubeGridSize ; j++){
			vec3 randomLocation = linearRand(vec3(-100, 0,-100), vec3(100, 0,100));
			vec3 randomDimension = linearRand(vec3(10, 10,10), vec3(70, 40,70));
			Cube * cube = new Cube(randomLocation, randomDimension, cubeMaterial, true);
			scene.addEntity(cube, & phongRenderer);
		}
	}

	cubeGridSize = 4;

	for(int i=0; i < cubeGridSize ; i++){
		for(int j=0; j < cubeGridSize ; j++){
			vec3 randomLocation = linearRand(vec3(-100, 0,-100), vec3(100, 0,100));
			vec3 randomDimension = linearRand(vec3(10, 40,10), vec3(50, 70,50));
			Cube * cube = new Cube(randomLocation, randomDimension, cubeMaterial, true);
			scene.addEntity(cube, & phongRenderer);
		}
	}

	cubeGridSize = 4;

	for(int i=0; i < cubeGridSize ; i++){
		for(int j=0; j < cubeGridSize ; j++){
			vec3 randomLocation = linearRand(vec3(-100, 0,-100), vec3(100, 0,100));
			vec3 randomDimension = linearRand(vec3(10, 70,10), vec3(20, 100,20));
			Cube * cube = new Cube(randomLocation, randomDimension, cubeMaterial, true);
			scene.addEntity(cube, & phongRenderer);
		}
	}

	PortalGun portalGun(&scene);
	Player player(playerProperties, &portalGun, &scene);

	Camera * camera = new PlayerCamera(&player);

	Controles controles(player);

	scene.setCamera(camera);

	PortalRenderer portalRenderer(&portalGun, camera, &scene,  &glProgram);
	portalRenderer.setProjection(projection);

    FMOD::System     *system;
    FMOD::Sound      *stillAlive;
    FMOD::Channel    *channel = 0;

    FMOD::System_Create(&system);
    system->setOutput(FMOD_OUTPUTTYPE_ALSA);
    system->init(32, FMOD_INIT_NORMAL, 0);
    system->createSound("sounds/still_alive.mp3", FMOD_SOFTWARE, 0, &stillAlive);\
    stillAlive->setMode(FMOD_LOOP_OFF);
    system->playSound(FMOD_CHANNEL_FREE, stillAlive, 0, &channel);

    CurserRenderer curser(&glProgram);

    printf("start render loop\n");
	do {
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

		clock.update();
		float dt = clock.getDt();
		controles.handleInput();

		player.step(dt);

		//printf("fps: %f", 1/clock.getDt());

		portalRenderer.render();
		scene.setCamera(camera);
		scene.render();
		curser.render();

		glfwSwapBuffers();
	} while( glfwGetKey( GLFW_KEY_ESC ) != GLFW_PRESS && glfwGetWindowParam( GLFW_OPENED ) );
	printf("exiting");
}

