/*
 * Scene.cpp
 *
 *  Created on: Jun 7, 2013
 *      Author: alex
 */

#include "Scene.h"

Scene::Scene() {
	// TODO Auto-generated constructor stub
}

Scene::~Scene() {
	// TODO Auto-generated destructor stub
}

void Scene::addEntity(Entity * entity, GLRenderer * renderer) {
	renderer->setProjection(projection);
	renderer->setCamera(camera);
	RenderingPair pair;
	pair.renderer = renderer;
	pair.entity = entity;
	renderedEntities.push_back(pair);
}

void Scene::setProjection(mat4 projection) {
	this->projection = projection;

	for(unsigned int i = 0; i< renderedEntities.size(); i++)
		renderedEntities[i].renderer->setProjection(projection);
}

void Scene::setCamera(Camera* camera) {
	this->camera = camera;

	for(unsigned int i = 0; i< renderedEntities.size(); i++)
		renderedEntities[i].renderer->setCamera(camera);
}

void Scene::render() {
	for(unsigned int i = 0; i<renderedEntities.size(); i++){
		GLRenderer * renderer = renderedEntities[i].renderer;
		Entity & entity = *renderedEntities[i].entity;
		fflush(stdout);
		renderer->render(entity);
	}
}

bool Scene::intersectsWithLine(vec3 start, vec3 end, vec3& position, vec3 & normal) {
	vec3 intersectionPosition;
	vec3 instersectionNormal;
	if(intersectsWithRay(start, end - start, intersectionPosition, instersectionNormal)){
		float t = length(intersectionPosition - start)/length(end - start);

		if(t > 0 && t < 1){
			position = intersectionPosition;
			normal = instersectionNormal;
			return true;
		}
	}

	return false;
}

mat4 Scene::getProjection() {
	return projection;
}

bool Scene::intersectsWithRay(vec3 origin, vec3 dir, vec3& position, vec3 & normal) {
	vector<vec3> intersectionsPositions;

	float minDistance = FLT_MAX;
	vec3 closestIntersectionPoint, currentIntersectionPoint;
	vec3 closestIntersectionNormal, currentIntersectionNormal;

	bool found = false;
	for(unsigned int i = 0; i< renderedEntities.size(); i++){
		vec3 position;
		Entity * entity = renderedEntities[i].entity;
		if(entity->intersectsWithRay(origin, dir, currentIntersectionPoint,currentIntersectionNormal)){
			found = true;
			float distance = length(currentIntersectionPoint - origin);
			if(distance < minDistance){
				minDistance = distance;
				closestIntersectionPoint = currentIntersectionPoint;
				closestIntersectionNormal = currentIntersectionNormal;
			}
		}
	}

	if(found){
		position = closestIntersectionPoint;
		normal = closestIntersectionNormal;
		return true;
	}

	return false;
}
