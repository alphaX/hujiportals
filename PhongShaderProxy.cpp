/*
 * WallShaderProxy.cpp
 *
 *  Created on: May 31, 2013
 *      Author: alex
 */

#include "PhongShaderProxy.h"

PhongShaderProxy::PhongShaderProxy(CGcontext cgContext, const char * fxFilepath):ShaderProxy(cgContext, fxFilepath) {
	vertexShader = getVertexProgram("vs");
	fragmentShader = getFragmentProgram("fs");

	shaderWvp = getParameter(vertexShader, "wvp");
	shaderW = getParameter(vertexShader, "w");
	shaderWIT = getParameter(vertexShader, "wIT");
	shaderPosition = getParameter(vertexShader, "pos");
	shaderNormals = getParameter(vertexShader, "normal");
	shaderColor = getParameter(fragmentShader, "color");
	shaderKd = getParameter(fragmentShader, "kd");
}

PhongShaderProxy::~PhongShaderProxy() {
	// TODO Auto-generated destructor stub
}

void PhongShaderProxy::setWvp(mat4 & w, mat4 & v, mat4 & p) {
	mat4 wvp = p*v*w;
	mat4 wIT = transpose(inverse(w));
	cgGLSetMatrixParameterfc(shaderWvp, value_ptr(wvp));
	cgGLSetMatrixParameterfc(shaderW, value_ptr(w));
	cgGLSetMatrixParameterfc(shaderWIT, value_ptr(wIT));
}

void PhongShaderProxy::setPosition(GLuint verticesBuffer) {
	glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer);
	cgGLEnableClientState(shaderPosition);
	cgGLSetParameterPointer(shaderPosition, 3, GL_FLOAT, 0, 0);
}

void PhongShaderProxy::enable() {
	cgGLBindProgram(vertexShader);
	cgGLEnableProfile(cgVprofile);

	cgGLBindProgram(fragmentShader);
	cgGLEnableProfile(cgFprofile);
}

void PhongShaderProxy::setNormals(GLuint normalBuffer) {
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
	cgGLEnableClientState(shaderNormals);
	cgGLSetParameterPointer(shaderNormals, 3, GL_FLOAT, 0, 0);
}

void PhongShaderProxy::setColor(vec3 color) {
	cgGLSetParameter3fv(shaderColor, value_ptr(color));
}

void PhongShaderProxy::setKd(float kd) {
	cgGLSetParameter1f(shaderKd, kd);
}

void PhongShaderProxy::clean() {
	cgGLDisableClientState(shaderPosition);
	cgGLDisableClientState(shaderNormals);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgFprofile);
}
