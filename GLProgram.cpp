/*
 * GLProgram.cpp
 *
 *  Created on: Apr 26, 2013
 *      Author: alex
 */

#include "GLProgram.h"
#include <stdio.h>
#include <stdlib.h>

CGcontext c;
GLProgram::GLProgram(const char * title) {
	initGL(title);
	initCG();

	GLFWvidmode return_struct;
	glfwGetDesktopMode( &return_struct );
	winWidth = return_struct.Width;
	winHeight = return_struct.Height;
}

GLProgram::~GLProgram() {
	// TODO Auto-generated destructor stub
}

CGcontext GLProgram::getContext(){
	return cgContext;
}

void GLProgram::initGL(const char * title){
	if( !glfwInit() ) {
			fprintf( stderr, "Failed to initialise GLFW\n" );
			exit(EXIT_FAILURE);
		}

	glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	glfwOpenWindowHint(GLFW_OPENGL_FORWARD_COMPAT,GL_FALSE);

	// Open a window and create its OpenGL context
	if( !glfwOpenWindow( winWidth, winHeight, 0,0,0,0, 32,32, GLFW_FULLSCREEN ) ) {
	//if( !glfwOpenWindow( winWidth, winHeight, 0,0,0,0, 32,0, GLFW_WINDOW ) ) {
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible.\n" );
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// Initialise GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialise GLEW\n");
		exit(EXIT_FAILURE);
	}
	if (!glewIsSupported(
			"GL_VERSION_2_0 "
			"GL_ARB_vertex_program "
			"GL_NV_gpu_program4 "
			"GL_NV_transform_feedback "
			"GL_ARB_texture_float "
	))	{
		printf("Unable to load extensions\n\nExiting...\n");
		exit(-1);
	}

	glfwSetWindowTitle( title);
	glEnable(GL_DEPTH_TEST);
	// Ensure we can capture the escape key being pressed below
	glfwEnable( GLFW_STICKY_KEYS );
	//glfwDisable(GLFW_MOUSE_CURSOR);
	glClearColor(0.1f, 0.1f, 0.3f, 0.0f);
	//glEnable(GL_CULL_FACE);
}

void cgErrorCallback() {
	CGerror lastError = cgGetError();
	if(lastError) 	{
		printf("cg error: %s\n", cgGetErrorString(lastError));
		printf("%s\n", cgGetLastListing(c));
		exit(1);
	}
}

void GLProgram::initCG(){
	if(!(cgContext = cgCreateContext())) {
		fprintf(stderr, "Failed to create Cg Context\n");
		exit(EXIT_FAILURE);
	}

	cgSetErrorCallback(cgErrorCallback);
	cgGLRegisterStates(cgContext);
	c = cgContext;

	printf("vertex profile:   %s\n", cgGetProfileString(cgGLGetLatestProfile(CG_GL_VERTEX)));
	printf("geometry profile: %s\n", cgGetProfileString(cgGLGetLatestProfile(CG_GL_GEOMETRY)));
	printf("fragment profile: %s\n", cgGetProfileString(cgGLGetLatestProfile(CG_GL_FRAGMENT)));
	printf("tessellation control profile: %s\n", cgGetProfileString(cgGLGetLatestProfile(CG_GL_TESSELLATION_CONTROL)));
	printf("tessellation evaluation profile: %s\n", cgGetProfileString(cgGLGetLatestProfile(CG_GL_TESSELLATION_EVALUATION)));

	CGprofile cgVertexProfile;
	if((cgVertexProfile = cgGLGetLatestProfile(CG_GL_VERTEX)) == CG_PROFILE_UNKNOWN) {
		fprintf(stderr, "Invalid profile type\n");
		exit(EXIT_FAILURE);
	}

	cgGLSetContextOptimalOptions(cgContext, cgVertexProfile);
}

int GLProgram::getWinHeight(){
	return winHeight;
}

int GLProgram::getWinWidth(){
	return winWidth;
}
