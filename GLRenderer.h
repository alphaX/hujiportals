/*
 * GLRenderer.h
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#ifndef GLRENDERER_H_
#define GLRENDERER_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include "Entity.h"
#include "Camera.h"

class GLRenderer {
public:
	GLRenderer();
	virtual ~GLRenderer();

	virtual void setProjection(glm::mat4 projection);
	virtual mat4 getProjection();
	virtual void setCamera(Camera * c);
	virtual void render(Entity & entity) = 0;
protected:
	glm::mat4 projection;
	Camera * camera;
};

#endif /* GLRENDERER_H_ */
