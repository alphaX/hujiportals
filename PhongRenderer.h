/*
 * WallRenderer.h
 *
 *  Created on: May 31, 2013
 *      Author: alex
 */

#ifndef WALLRENDERER_H_
#define WALLRENDERER_H_

#include "PhongShaderProxy.h"
#include "Camera.h"
#include "Entity.h"
#include "GLRenderer.h"
#include "PhongShaderProxy.h"
#include "Cube.h"
#include "Material.h"

class PhongRenderer: public GLRenderer {
public:
	PhongRenderer(CGcontext cgContext);
	virtual ~PhongRenderer();

	void render(Entity & e);
private:
	PhongShaderProxy * shaderProxy;
};

#endif /* WALLRENDERER_H_ */
