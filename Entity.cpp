/*
 * Entity.cpp
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#include "Entity.h"

Entity::Entity(){
}

Entity::~Entity() {
}

GLuint Entity::getVerticesBuffer() {
	return mesh->getVerticesBuffer();
}

GLuint Entity::getNormalBuffer() {
	return mesh->getNormalBuffer();
}

unsigned int Entity::getNumberOfVertices() {
	return mesh->getNumberOfVertices();
}

void Entity::setMaterial(Material material) {
	this->material = material;
}

Material Entity::getMaterial() {
	return material;
}
