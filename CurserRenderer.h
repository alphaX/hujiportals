/*
 * CurserRenderer.h
 *
 *  Created on: Jun 21, 2013
 *      Author: alex
 */

#ifndef CURSERRENDERER_H_
#define CURSERRENDERER_H_

#include "GLProgram.h"
#include "BufferedMesh.h"
#include "FlatShaderProxy.h"
#include <glm/glm.hpp>

using namespace glm;
using namespace std;

class CurserRenderer {
public:
	CurserRenderer(GLProgram * program);
	virtual ~CurserRenderer();

	void render();
private:
	BufferedMesh * curser;
	FlatShaderProxy * shaderProxy;
};

#endif /* CURSERRENDERER_H_ */
