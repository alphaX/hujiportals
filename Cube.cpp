/*
 * Cube.cpp
 *
 *  Created on: Jun 1, 2013
 *      Author: alex
 */

#include "Cube.h"

Cube::Cube(vec3 location, vec3 dimension, Material material, bool outer) {
	this->location = location;
	this->dimension = dimension;
	this->material = material;
	this->outer = outer;

	createCubeMesh(outer);
}

Cube::~Cube() {
	// TODO Auto-generated destructor stub
}

mat4 Cube::getWorldMatrix() {
	mat4 world = scale(
			translate(mat4(1), location),
			dimension);
	return world;
}

bool Cube::intersectsWithLine(vec3 start, vec3 end, vec3& position, vec3 & normal) {
	return false;
}

bool Cube::intersectsWithRay(vec3 origin, vec3 dir, vec3& position, vec3 & normal) {
	vec3 v0 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(1, -1, -1, 1));
	vec3 v1 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(1, -1, 1, 1));
	vec3 v2 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(-1, -1, 1, 1));
	vec3 v3 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(-1, -1, -1, 1));
	vec3 v4 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(1, 1, -1, 1));
	vec3 v5 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(1, 1, 1, 1));
	vec3 v6 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(-1, 1, 1, 1));
	vec3 v7 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(-1, 1, -1, 1));

	vector<vec3> positions;
	vector<vec3> normals;

	vec3 current_position;

	int outFacingDir = outer ? -1 : 1;
	// bottom
	if(Intersections::rectWithRay(v0, v1, v2, v3, origin ,dir, current_position)){
		positions.push_back(current_position);
		normals.push_back(vec3(0, 1 * outFacingDir, 0));
	}

	// top
	if(Intersections::rectWithRay(v4, v5, v6, v7, origin ,dir, current_position)){
		positions.push_back(current_position);
		normals.push_back(vec3(0, -1 * outFacingDir, 0));
	}

	// back
	if(Intersections::rectWithRay(v0, v4, v7, v3, origin ,dir, current_position)){
		positions.push_back(current_position);
		normals.push_back(vec3(0, 0, 1 * outFacingDir));
	}

	// front
	if(Intersections::rectWithRay(v1, v5, v6, v2, origin ,dir, current_position)){
		positions.push_back(current_position);
		normals.push_back(vec3(0, 0, -1 * outFacingDir));
	}

	// left
	if(Intersections::rectWithRay(v2, v6, v7, v3, origin ,dir, current_position)){
		positions.push_back(current_position);
		normals.push_back(vec3(1 * outFacingDir, 0, 0));
	}

	// right
	if(Intersections::rectWithRay(v0, v1, v5, v4, origin ,dir, current_position)){
		positions.push_back(current_position);
		normals.push_back(vec3(-1 * outFacingDir, 0, 0));
	}

	if(positions.size() == 0)
		return false;

	vec3 closestPosition = positions[0];
	vec3 closestNormal = normals[0];
	float minDistance = length(positions[0]-origin);

	for(unsigned int i = 0; i < positions.size(); i++){
		if(length(positions[i] - origin) < minDistance){
			closestPosition = positions[i];
			closestNormal = normals[i];
			minDistance = length(positions[i] - origin);
		}
	}

	position = closestPosition;
	normal = closestNormal;
	return true;
}

void Cube::createCubeMesh(bool outer) {
	vector<vec3> vetrices;
	vector<vec3> normals;
	int outerDir = outer ? -1 : 1;
	// bottom
	vetrices.push_back(vec3(1.0f, -1.0f, -1.0f));
	vetrices.push_back(vec3(1.0f, -1.0f, 1.0f));
	vetrices.push_back(vec3(-1.0f, -1.0f, 1.0f));
	vetrices.push_back(vec3(1.0f, -1.0f, -1.0f));
	vetrices.push_back(vec3(-1.0f, -1.0f, 1.0f));
	vetrices.push_back(vec3(-1.0f, -1.0f, -1.0f));
	for (int i = 0; i < 6; i++)
		normals.push_back(vec3(0, 1 * outerDir, 0));
	// top
	vetrices.push_back(vec3(1.0f, 1.0f, -1.0f));
	vetrices.push_back(vec3(1.0f, 1.0f, 1.0f));
	vetrices.push_back(vec3(-1.0f, 1.0f, 1.0f));
	vetrices.push_back(vec3(1.0f, 1.0f, -1.0f));
	vetrices.push_back(vec3(-1.0f, 1.0f, 1.0f));
	vetrices.push_back(vec3(-1.0f, 1.0f, -1.0f));
	for (int i = 0; i < 6; i++)
		normals.push_back(vec3(0, -1 * outerDir, 0));
	// back
	vetrices.push_back(vec3(1.0f, -1.0f, 1.0f));
	vetrices.push_back(vec3(-1.0f, -1.0f, 1.0f));
	vetrices.push_back(vec3(1.0f, 1.0f, 1.0f));
	vetrices.push_back(vec3(1.0f, 1.0f, 1.0f));
	vetrices.push_back(vec3(-1.0f, -1.0f, 1.0f));
	vetrices.push_back(vec3(-1.0f, 1.0f, 1.0f));
	for (int i = 0; i < 6; i++)
		normals.push_back(vec3(0, 0, -1 * outerDir));
	// front
	vetrices.push_back(vec3(1.0f, -1.0f, -1.0f));
	vetrices.push_back(vec3(-1.0f, -1.0f, -1.0f));
	vetrices.push_back(vec3(1.0f, 1.0f, -1.0f));
	vetrices.push_back(vec3(1.0f, 1.0f, -1.0f));
	vetrices.push_back(vec3(-1.0f, -1.0f, -1.0f));
	vetrices.push_back(vec3(-1.0f, 1.0f, -1.0f));
	for (int i = 0; i < 6; i++)
		normals.push_back(vec3(0, 0, 1 * outerDir));
	// left
	vetrices.push_back(vec3(-1.0f, -1.0f, 1.0f));
	vetrices.push_back(vec3(-1.0f, 1.0f, 1.0f));
	vetrices.push_back(vec3(-1.0f, -1.0f, -1.0f));
	vetrices.push_back(vec3(-1.0f, 1.0f, 1.0f));
	vetrices.push_back(vec3(-1.0f, -1.0f, -1.0f));
	vetrices.push_back(vec3(-1.0f, 1.0f, -1.0f));
	for (int i = 0; i < 6; i++)
		normals.push_back(vec3(1 * outerDir, 0, 0));
	// right
	vetrices.push_back(vec3(1.0f, -1.0f, 1.0f));
	vetrices.push_back(vec3(1.0f, 1.0f, 1.0f));
	vetrices.push_back(vec3(1.0f, -1.0f, -1.0f));
	vetrices.push_back(vec3(1.0f, 1.0f, 1.0f));
	vetrices.push_back(vec3(1.0f, -1.0f, -1.0f));
	vetrices.push_back(vec3(1.0f, 1.0f, -1.0f));
	for (int i = 0; i < 6; i++)
		normals.push_back(vec3(-1 * outerDir, 0, 0));
	mesh = new BufferedMesh(vetrices, normals);
}

