/*
 * BufferedMesh.cpp
 *
 *  Created on: May 31, 2013
 *      Author: alex
 */

#include "BufferedMesh.h"

BufferedMesh::BufferedMesh(vector<vec3> & vertices, vector<vec3>& normals) {
	buffer(vertices, normals);
	numberOfVertices = vertices.size();
}

BufferedMesh::~BufferedMesh() {
	glDeleteBuffers(1, &(verticesBuffer));
	glDeleteBuffers(1, &(normalsBuffer));
}

void BufferedMesh::bindVerticesBuffers() {
	glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer);
}

void BufferedMesh::bindNormalBuffers() {
	glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer);
}

GLuint BufferedMesh::getVerticesBuffer() {
	return verticesBuffer;
}

GLuint BufferedMesh::getNormalBuffer() {
	return normalsBuffer;
}

unsigned int BufferedMesh::getNumberOfVertices() {
	return numberOfVertices;
}

void BufferedMesh::buffer(vector<vec3> & vertices, vector<vec3> & normals) {
	glGenBuffers(1, &verticesBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	//printf("buffering entity with %u vertices starting with: (%f, %f, %f) buffer is %u\n", vertices.size(), vertices.at(0).x, vertices.at(0).y, vertices.at(0).z, verticesBuffer);

	glGenBuffers(1, &normalsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
}
