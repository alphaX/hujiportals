/*
 * PortalCamera.h
 *
 *  Created on: Jun 1, 2013
 *      Author: alex
 */

#ifndef PORTALCAMERA_H_
#define PORTALCAMERA_H_

#include "Camera.h"
#include "Portal.h"

class PortalCamera : public Camera {
public:
	PortalCamera(Portal * srcPortal, Portal * dstPortal, Camera * mainCamera);
	virtual ~PortalCamera();

	mat4 getViewMatrix();
private:
	Portal * srcPortal;
	Portal * dstPortal;
	Camera * mainCamera;
};

#endif /* PORTALCAMERA_H_ */
