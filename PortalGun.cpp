/*
 * PortalGun.cpp
 *
 *  Created on: Jun 8, 2013
 *      Author: alex
 */

#include "PortalGun.h"

PortalGun::PortalGun(Scene * world) {
	// TODO Auto-generated constructor stub
	this->world = world;
	orangePortal = NULL;
	bluePortal = NULL;

	defaultPortalDimension = vec2(5,5);

	FMOD::System_Create(&fmodSystem);
	fmodSystem->setOutput(FMOD_OUTPUTTYPE_ALSA);
	fmodSystem->init(32, FMOD_INIT_NORMAL, 0);
	fmodSystem->createSound("sounds/psy.wav", FMOD_SOFTWARE, 0, &portalSound);
	portalSound->setMode(FMOD_LOOP_OFF);
}

PortalGun::~PortalGun() {
	// TODO Auto-generated destructor stub
}


void PortalGun::fireAt(vec3 fromPosition, vec3 direction, PortalIndex portalIndex) {

	Portal * portal = getPortal(portalIndex);
	vec3 intersectionPoint;
	vec3 intersectionNormal;
	bool intersects = world->intersectsWithRay(fromPosition, direction, intersectionPoint, intersectionNormal);

	if(intersects){
		fmodSystem->playSound(FMOD_CHANNEL_FREE, portalSound, 0, &channel);
		if(!portalExists(portalIndex))
			createPortal(portalIndex, intersectionPoint + intersectionNormal * 0.05f, intersectionNormal);
		else{
			vec3 portalPosition = intersectionPoint + intersectionNormal * 0.05f;
			portal->setLocation(portalPosition);
			portal->setNormal(intersectionNormal);
		}
	}
}

Portal * PortalGun::getPortal(PortalIndex portal) {
	switch(portal){
	case ORANGE:
		return orangePortal;
	case BLUE:
		return bluePortal;
	}

	return NULL;
}

bool PortalGun::portalExists(PortalIndex portal) {
	return getPortal(portal) != NULL;
}

void PortalGun::createPortal(PortalIndex portal, vec3 location, vec3 normal) {
	switch(portal){
	case ORANGE:
		orangePortal = new Portal(location, defaultPortalDimension, normal);
		break;
	case BLUE:
		bluePortal = new Portal(location, defaultPortalDimension, normal);
		break;
	}
}
