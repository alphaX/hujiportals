/*
 * ShaderProxy.h
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#ifndef SHADERPROXY_H_
#define SHADERPROXY_H_


#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <Cg/cgGL.h>

class ShaderProxy {
public:
	ShaderProxy(CGcontext cgContext, const char * fxFilepath);
	virtual ~ShaderProxy();

	CGparameter getParameter(CGprogram & program, const char * name);
	CGprogram getVertexProgram(const char * programName);
	CGprogram getFragmentProgram(const char * programName);
	CGprogram getGeometryProgram(const char * programName);

	virtual void enable() = 0;
	virtual void clean() = 0;

protected:
	const char * fxFilepath;
	CGcontext cgContext;
	CGprofile cgVprofile, cgFprofile, cgGprofile;
	CGprogram getProgram(const char * programName, CGprofile);
};

#endif /* SHADERPROXY_H_ */
