/*
 * Intersections.cpp
 *
 *  Created on: Jun 8, 2013
 *      Author: alex
 */

#include "Intersections.h"

bool Intersections::rectWithRay(vec3 vert0, vec3 vert1, vec3 vert2, vec3 vert3,
		vec3 orig, vec3 dir, vec3& position) {

		vec3 position1, position2 ;
		bool res1 = intersectLineTriangle(orig,
			dir,
			vert0,
			vert1,
			vert2,
			position1
		);

		bool res2 = intersectLineTriangle(orig,
			dir,
			vert0,
			vert2,
			vert3,
			position2
		);

		if (res1 &&  position1.x > 0){
			// position1 = (u, v, t)
			// p + t * d = (1-u-v) * p0 + u * p1 + v * p2
			position = orig + dir * position1.x;
			return true;
		}
		if(res2 && position2.x > 0){
			position = orig + dir * position2.x;
			return true;
		}

		return false;
}

bool Intersections::rectWithLine(vec3 vert0, vec3 vert1, vec3 vert2, vec3 vert3, vec3 start,
		vec3 end, vec3& position) {
	if( length(end - start) < 0.0001)
		return false;

	vec3 orig = start;
	vec3 dir = normalize(end - start);

	vec3 rayIntersectionPosition;
	bool res = rectWithRay(vert0, vert1, vert2, vert3, orig, dir, rayIntersectionPosition);

	float t = length(rayIntersectionPosition - start)/length(end - start);
	if (res && t > 0 && t <1){
		position = rayIntersectionPosition;
		return true;
	}
	return false;
}
