/*
 * Entity.h
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#ifndef ENTITY_H_
#define ENTITY_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include <vector>

#include "BufferedMesh.h"
#include "Material.h"

using namespace glm;

class Entity {
public:
	Entity();
	virtual ~Entity();

	virtual mat4 getWorldMatrix() = 0;
	GLuint getVerticesBuffer();
	GLuint getNormalBuffer();
	unsigned int getNumberOfVertices();

	void setMaterial(Material material);
	Material getMaterial();

	virtual bool intersectsWithLine(vec3 start, vec3 end, vec3 & position, vec3 & normal) = 0;
	virtual bool intersectsWithRay(vec3 origin, vec3 dir, vec3 & position, vec3 & normal) = 0;
protected:
	BufferedMesh * mesh;
	Material material;
};

#endif /* ENTITY_H_ */
