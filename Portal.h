/*
 * Portal.h
 *
 *  Created on: Jun 1, 2013
 *      Author: alex
 */

#ifndef PORTAL_H_
#define PORTAL_H_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/intersect.hpp>
#include <glm/gtc/swizzle.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include "Entity.h"
#include "Intersections.h"

using namespace glm;

class Portal: public Entity {
public:
	Portal(vec3 location, vec2 dimansion, vec3 normal);
	virtual ~Portal();
	vec3 getLocation();
	void setLocation(vec3 location);
	void setNormal(vec3 normal);
	vec3 getNormal();
	mat4 getWorldMatrix();
	vec2 getDimension();
	bool intersectsWithLine(vec3 start, vec3 end, vec3 & position, vec3 & normal);
	bool intersectsWithRay(vec3 origin, vec3 dir, vec3 & position, vec3 & normal);
private:
	vec3 location;
	vec2 dimension;
	vec3 normal;

	void createPortalMesh();
};

#endif /* PORTAL_H_ */
