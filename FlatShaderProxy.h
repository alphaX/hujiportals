/*
 * flatShaderProxy.h
 *
 *  Created on: Jun 21, 2013
 *      Author: alex
 */

#ifndef FLATSHADERPROXY_H_
#define FLATSHADERPROXY_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include "ShaderProxy.h"

class FlatShaderProxy: public ShaderProxy {
public:
	FlatShaderProxy(CGcontext cgContext, const char * fxFilepath);
	virtual ~FlatShaderProxy();
	void setPosition(GLuint verticesBuffer);
	void enable();
	void clean();
private:
	CGprogram vertexShader, fragmentShader;
	CGparameter shaderPosition;
};

#endif /* FLATSHADERPROXY_H_ */
