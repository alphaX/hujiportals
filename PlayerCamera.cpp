/*
 * PlayerCamera.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */

#include "PlayerCamera.h"
#define EPSILON 0.01


PlayerCamera::PlayerCamera(Player * player){
	this->player = player;
}

glm::mat4 PlayerCamera::getViewMatrix(){
//	glm::mat4 viewMatrix(1);
//
//	glm::vec3 position = this->player->getPosition();
//	viewMatrix = glm::translate(-position.x, -position.y, -position.z) * viewMatrix;
//
//	glm::mat4 lookat = glm::lookAt(glm::vec3(0.0f,0.0f,0.0f), player->getCurrentTurn(), glm::vec3(0.0f,1.0f,0.0f));
//	viewMatrix = lookat * viewMatrix;
//	return viewMatrix;
//
	glm::vec3 position = this->player->getPosition();
	glm::mat4 translation = glm::translate(-position.x, -position.y, -position.z);

	glm::mat4 rotation = glm::lookAt(glm::vec3(0.0f,0.0f,0.0f), player->getCurrentTurn(), glm::vec3(0.0f,1.0f,0.0f));
	return rotation * translation;
}

glm::vec3 PlayerCamera::getPosition() {
	return player->getPosition();
}
