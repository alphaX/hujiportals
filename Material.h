/*
 * Material.h
 *
 *  Created on: Jun 1, 2013
 *      Author: alex
 */

#ifndef MATERIAL_H_
#define MATERIAL_H_

#include <glm/glm.hpp>
using namespace glm;

class Material {
public:
	Material();
	virtual ~Material();

	vec3 color;
	float kDiffuse;
};

#endif /* MATERIAL_H_ */
