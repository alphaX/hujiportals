/*
 * player.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "Player.h"

Player::Player(PlayerPropeties properties, PortalGun * portalGun, Scene * scene){
	this->properties = properties;
	this->turnDirection = glm::vec3(0,0,1);
	this->position = glm::vec3(0,120,-3);
	this->currentSelfForce = glm::vec3(0,0,0);
	this->portalGun = portalGun;
	this->scene = scene;

	FMOD::System_Create(&fmodSystem);
	fmodSystem->setOutput(FMOD_OUTPUTTYPE_ALSA);
	fmodSystem->init(32, FMOD_INIT_NORMAL, 0);
	fmodSystem->createSound("sounds/warp.wav", FMOD_SOFTWARE, 0, &warpSound);\
	warpSound->setMode(FMOD_LOOP_OFF);
}

void Player::advance(Direction direction){
	bool god = false;
	this->direction = direction;

	if(god){
		switch(direction){
		case FORWARD:
			this->currentSelfForce = this->turnDirection*this->properties.speed;
			break;
		case BACKWARDS:
			this->currentSelfForce = -this->turnDirection*this->properties.speed;
			break;
		case LEFT:
			this->currentSelfForce = glm::rotate(glm::vec3(this->turnDirection.x, 0, this->turnDirection.z), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f))*this->properties.speed;
			break;
		case RIGHT:
			this->currentSelfForce = glm::rotate(glm::vec3(this->turnDirection.x, 0, this->turnDirection.z), -90.0f, glm::vec3(0.0f, 1.0f, 0.0f))*this->properties.speed;
			break;
		case UP:
			this->currentSelfForce = glm::vec3(0.0f, 1.0f, 0.0f)*this->properties.speed;
			break;
		case DOWN:
			this->currentSelfForce = -glm::vec3(0.0f, 1.0f, 0.0f)*this->properties.speed;
			break;
		}
	} else{


		vec3 flatDirection = vec3(turnDirection.x, 0, turnDirection.z);
		switch(direction){

		case FORWARD:
			this->currentSelfForce = flatDirection*this->properties.speed;
			break;
		case BACKWARDS:
			this->currentSelfForce = -flatDirection*this->properties.speed;
			break;
		case LEFT:
			this->currentSelfForce = glm::rotate(flatDirection, 90.0f, glm::vec3(0.0f, 1.0f, 0.0f))*this->properties.speed;
			break;
		case RIGHT:
			this->currentSelfForce = glm::rotate(flatDirection, -90.0f, glm::vec3(0.0f, 1.0f, 0.0f))*this->properties.speed;
			break;
		case UP:
			if(onFloor){
				this->currentSelfForce = glm::vec3(0.0f, 1.0f, 0.0f)*this->properties.jumpSpeed;
				onFloor = false;
			}
			break;
		case DOWN:
			//this->currentSelfForce = -glm::vec3(0.0f, 1.0f, 0.0f)*this->properties.speed;
			break;
		}
	}
}

void Player::turn(float turnX, float turnY){
	glm::vec3 axisX = glm::vec3(0,1,0);
	glm::vec3 axisY = glm::cross(this->turnDirection, glm::vec3(0,1,0));
	this->turnDirection = glm::rotate(this->turnDirection, turnX*this->properties.angleSpeed, axisX);

	this->turnDirection = glm::rotate(this->turnDirection, turnY*this->properties.angleSpeed, axisY);
}

glm::vec3  Player::getPosition(){
	return this->position;
}

void Player::step(float dt){
	vec3 startPosition = this->position;
	vec3 gravity = vec3(0,-1,0);

	updateOnFloor();

	vec3 currentGravity = onFloor? vec3(0) : gravity;

	currentSpeed = currentSpeed + (currentSelfForce + currentGravity)*dt;

	this->position += currentSpeed;
	vec3 endPosition = this->position;

	// we don't want the 'self force' to couse acceleration so we'll substruct it now
	currentSpeed.x = currentSpeed.x - currentSelfForce.x*dt;
	currentSpeed.z = currentSpeed.z - currentSelfForce.z*dt;
	currentSelfForce = vec3(0);

	if(!wrapThroughPortals(startPosition, endPosition))
		collideWithScene(startPosition, endPosition);
}

float Player::getHeight(){
	return this->properties.height;
}

glm::vec3 Player::getCurrentTurn(){
	return this->turnDirection;
}

void Player::setPosition(glm::vec3 position) {
	this->position = position;
}

void Player::setTurnDirection(glm::vec3 turn) {
	turnDirection = turn;
}

void Player::firePortal(PortalIndex portalIndex) {
	portalGun->fireAt(position, turnDirection, portalIndex);
}

void Player::collideWithScene(vec3 startPosition, vec3 endPosition) {
	vec3 collisionPoint, normalAtCollision;
	if(scene->intersectsWithLine(startPosition, endPosition, collisionPoint, normalAtCollision)){
		if(normalAtCollision.y > 0.01){
			onFloor = true;
			currentSpeed = vec3(0);
			setPosition(collisionPoint + normalAtCollision*getHeight());
		} else{
			currentSpeed.x = 0;
			currentSpeed.z = 0;
			setPosition(collisionPoint + normalAtCollision*OFFSET_FROM_SOLID);
		}
	}
}


bool Player::wrapThroughPortals(vec3 startPosition, vec3 endPosition) {
	Portal * intersectingPortal, * otherPortal;
	vec3 intersectionPoint;
	if(findIntersectingPortal(startPosition, endPosition, intersectingPortal, otherPortal, intersectionPoint)){
		fmodSystem->playSound(FMOD_CHANNEL_FREE, warpSound,0, &channel);
		mat4 warpingMatrix = createWarpingMatrix(intersectingPortal, otherPortal);

		vec3 warpingDest = swizzle<X,Y,Z>(warpingMatrix * vec4(endPosition,1));

		vec3 warping0 = swizzle<X,Y,Z>(warpingMatrix * vec4(0,0,0,1));
		vec3 warpingDirection1 = swizzle<X,Y,Z>(warpingMatrix * vec4(getCurrentTurn(),1));
		vec3 warpingSpeed1 = swizzle<X,Y,Z>(warpingMatrix * vec4(currentSpeed,1));

		vec3 warpingDirectrion = normalize(warpingDirection1 - warping0);
		vec3 warpingSpeed = warpingSpeed1 - warping0;

		setPosition(warpingDest);
		setTurnDirection(warpingDirectrion);

		currentSpeed = warpingSpeed;
		return true;
	}
	return false;
}

mat4 Player::createWarpingMatrix(Portal * srcPortal, Portal * dstPortal){
	glm::mat4 portalView =
		srcPortal->getWorldMatrix()
		* glm::rotate(glm::mat4(1.0), 180.0f, glm::vec3(0.0,1.0,0.0))
		* glm::inverse(dstPortal->getWorldMatrix())
		;

	return inverse(portalView);
}

bool Player::findIntersectingPortal(vec3 startPosition, vec3 endPosition, Portal * & intersectedPortal, Portal * & otherPortal, vec3 & intersectionPoint){
	if(portalGun->portalExists(ORANGE) && portalGun->portalExists(BLUE)){
		Portal * orangePortal = portalGun->getPortal(ORANGE);
		Portal * bluePortal = portalGun->getPortal(BLUE);
		vec3 position, normal;

		if(orangePortal->intersectsWithLine(startPosition, endPosition, position, normal)){
			intersectedPortal = orangePortal;
			otherPortal = bluePortal;
			intersectionPoint = position;
			//printf("instersection with orange portal: %p. other: %p \n", intersectedPortal, otherPortal);
			return true;
		} else if(bluePortal->intersectsWithLine(startPosition, endPosition, position, normal)){
			intersectedPortal = bluePortal;
			otherPortal = orangePortal;
			intersectionPoint = position;
			//printf("instersection with blue portal: %p. other: %p \n", intersectedPortal, otherPortal);
			return true;
		}
	}

	return false;
}

void Player::updateOnFloor() {
	vec3 collisionPoint, normalAtCollision;
	vec3 floorCheckOffset = vec3(0, -(getHeight() + 0.01), 0);

	if(!scene->intersectsWithLine(position, position + floorCheckOffset, collisionPoint, normalAtCollision)){
		onFloor = false;
	}

	Portal * intersectingPortal, * otherPortal;
	vec3 intersectionPoint;
	if(findIntersectingPortal(position, position + floorCheckOffset, intersectingPortal, otherPortal, intersectionPoint))
		onFloor = false;
}
