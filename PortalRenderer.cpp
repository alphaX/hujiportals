/*
 * PortalRenderer.cpp
 *
 *  Created on: Jun 1, 2013
 *      Author: alex
 */

#include "PortalRenderer.h"

PortalRenderer::PortalRenderer(PortalGun * portalGun, Camera* camera,
		Scene* scene, GLProgram * program) {
	this->portalGun = portalGun;
	this->camera = camera;
	this->scene = scene;
	this->program = program;
	portalShapeRenderer = new PhongRenderer(program->getContext());
}

PortalRenderer::~PortalRenderer() {
	// TODO Auto-generated destructor stub
}

void PortalRenderer::render() {
	portalShapeRenderer->setProjection(projection);
	portalShapeRenderer->setCamera(camera);

	bool orangeExists = portalGun->portalExists(ORANGE);
	bool blueExists = portalGun->portalExists(BLUE);

	if(!orangeExists && !blueExists ){
		return;
	} else	if(orangeExists && !blueExists){
		Portal * portal = portalGun->getPortal(ORANGE);
		renderSinglePortal(portal);
	} else	if(!orangeExists && blueExists){
		Portal * portal = portalGun->getPortal(BLUE);
		renderSinglePortal(portal);
	} else{ // both portal exist
		renderBothPortals();
	}
}

void PortalRenderer::renderBothPortals() {
	Portal* portal1 = portalGun->getPortal(ORANGE);
	Portal* portal2 = portalGun->getPortal(BLUE);

	renderInStencil(portal1);
	renderSceneThroughPortal(portal1, portal2);

	renderInStencil(portal2);
	renderSceneThroughPortal(portal2, portal1);

	glClear(GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
	renderInDepth(portal1);
	renderInDepth(portal2);

	//renderFrame(portal1);
}

void PortalRenderer::renderSceneThroughPortal(Portal * target, Portal * other) {
	vec3 worldCameraPosition = swizzle<X,Y,Z>(inverse(camera->getViewMatrix()) * vec4(0,0,0,1));
	float distanceToPortal = length(worldCameraPosition - target->getLocation());
	mat4 sceneProjection = scene->getProjection();

	mat4 portalProjection = perspective(45.0f,(float)program->getWinWidth() / program->getWinHeight(), distanceToPortal+other->getDimension().x, 500.0f);
	PortalCamera portalCamera(target, other, camera);
	// render scene through portal cameras
	scene->setProjection(portalProjection);
	scene->setCamera(&portalCamera);
	scene->render();

	scene->setProjection(sceneProjection);
	glDisable(GL_STENCIL_TEST);
}

void PortalRenderer::renderInStencil(Portal* portal) {
	GLboolean save_stencil_test;
	GLboolean save_color_mask[4];
	GLboolean save_depth_mask;
	glClear(GL_STENCIL_BUFFER_BIT);
	glGetBooleanv(GL_STENCIL_TEST, &save_stencil_test);

	glGetBooleanv(GL_COLOR_WRITEMASK, save_color_mask);
	glGetBooleanv(GL_DEPTH_WRITEMASK, &save_depth_mask);

	glEnable(GL_STENCIL_TEST);

	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glDepthMask(GL_FALSE);

	glStencilMask(0xFF);
	glClearStencil(0);
	glStencilFunc(GL_NEVER, 0, 0xFF);
	glStencilOp(GL_INCR, GL_KEEP, GL_KEEP);  // draw 1s on test fail (always)
	//glClear(GL_STENCIL_BUFFER_BIT);  // needs mask=0xFF

	portalShapeRenderer->render(*portal);

	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDepthMask(GL_TRUE);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	// Fill 1 or more
	glStencilFunc(GL_LEQUAL, 1, 0xFF);
	glColorMask(save_color_mask[0], save_color_mask[1], save_color_mask[2], save_color_mask[3]);
	glDepthMask(save_depth_mask);

}

void PortalRenderer::renderInDepth(Portal* portal) {
	GLboolean save_color_mask[4];
	GLboolean save_depth_mask;

	glGetBooleanv(GL_COLOR_WRITEMASK, save_color_mask);
	glGetBooleanv(GL_DEPTH_WRITEMASK, &save_depth_mask);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glDepthMask(GL_TRUE);
	portalShapeRenderer->render(*portal);

	glColorMask(save_color_mask[0], save_color_mask[1], save_color_mask[2], save_color_mask[3]);
	glDepthMask(save_depth_mask);
}

void PortalRenderer::renderSinglePortal(Portal* portal) {
	portalShapeRenderer->render(*portal);
}

void PortalRenderer::render(Entity& entity) {
}
