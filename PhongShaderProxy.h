/*
 * WallShaderProxy.h
 *
 *  Created on: May 31, 2013
 *      Author: alex
 */

#ifndef WALLSHADERPROXY_H_
#define WALLSHADERPROXY_H_

#include <GL/glew.h>
#include <GL/glfw.h>
#include "ShaderProxy.h"

using namespace glm;
using namespace std;
class PhongShaderProxy: public ShaderProxy {
public:
	PhongShaderProxy(CGcontext cgContext, const char * fxFilepath);
	virtual ~PhongShaderProxy();

	void setWvp(mat4 & w, mat4 & v, mat4 & p);
	void setPosition(GLuint verticesBuffer);
	void setNormals(GLuint normalBuffer);
	void setColor(vec3 color);
	void setKd(float kd);
	void enable();
	void clean();
private:
	CGprogram vertexShader, fragmentShader;
	CGparameter shaderWvp, shaderW, shaderWIT, shaderPosition, shaderNormals,
		shaderColor, shaderKd;

	mat4 wvp, wv;
};

#endif /* WALLSHADERPROXY_H_ */
