/*
 * Controles.h
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#ifndef CONTROLES_H_
#define CONTROLES_H_

#include <GL/glew.h>
#include <GL/glfw.h>
#include "Player.h"

class Controles {
public:
	Controles(Player & player);
	virtual ~Controles();

	void handleInput();
private:
	Player & player;
	float previouseMouseX;
	float previouseMouseY;
	bool firstFrame;
	bool orangePortalFired;
	bool bluePortalFired;
	void handleKeyboardInput();
	void handleMouseInput();
};

#endif /* CONTROLES_H_ */
