/*
 * Scene.h
 *
 *  Created on: Jun 7, 2013
 *      Author: alex
 */

#ifndef SCENE_H_
#define SCENE_H_
#include <vector>
#include <glm/glm.hpp>
#include <cfloat>

#include "GLRenderer.h"
#include "Camera.h"

using namespace std;
using namespace glm;

struct RenderingPair{
	GLRenderer * renderer;
	Entity * entity;
};

class Scene {
public:
	Scene();
	virtual ~Scene();
	void setProjection(mat4 projection);
	mat4 getProjection();
	void setCamera(Camera * camara);
	void addEntity(Entity * entity, GLRenderer * renderer);
	void render();

	bool intersectsWithLine(vec3 start, vec3 end, vec3 & position, vec3 & normal);
	bool intersectsWithRay(vec3 origin, vec3 dir, vec3 & position, vec3 & normal);
private:
	vector<RenderingPair> renderedEntities;
	mat4 projection;
	Camera * camera;
};

#endif /* SCENE_H_ */
