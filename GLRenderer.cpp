/*
 * GLRenderer.cpp
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#include "GLRenderer.h"

GLRenderer::GLRenderer() {}

GLRenderer::~GLRenderer() {}

void GLRenderer::setProjection(glm::mat4 projection) {
	this->projection = projection;
}

mat4 GLRenderer::getProjection() {
	return projection;
}

void GLRenderer::setCamera(Camera * c) {
	camera = c;
}
