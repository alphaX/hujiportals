/*
 * PlayerCamera.hpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */

#ifndef PlayerCamera_HPP_
#define PlayerCamera_HPP_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr
#include <glm/gtx/vector_angle.hpp>

#include "Player.h"
#include "Camera.h"

class PlayerCamera: public Camera{
	Player * player;
public:
	PlayerCamera(Player * player);

	vec3 getPosition();
	mat4 getViewMatrix();
};


#endif /* PlayerCamera_HPP_ */
