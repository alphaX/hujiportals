/*
 * Mesh.h
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#ifndef MESH_H_
#define MESH_H_
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

class Mesh {
public:
	Mesh(std::vector<vec3> & vertices, std::vector<uvec3> & indexes, std::vector<vec3> & normals);
	virtual ~Mesh();
	std::vector<vec3> & getVertices();
	std::vector<uvec3> & getIndexes();
	int getIndexesNumber();
private:
	std::vector<vec3> & vertices;
	std::vector<uvec3> & indexes;
	std::vector<vec3> & normals;
};

#endif /* MESH_H_ */
