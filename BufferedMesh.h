/*
 * BufferedMesh.h
 *
 *  Created on: May 31, 2013
 *      Author: alex
 */

#ifndef BUFFEREDMESH_H_
#define BUFFEREDMESH_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <vector>
#include <glm/glm.hpp>

using namespace std;
using namespace glm;

class BufferedMesh {
public:
	BufferedMesh(vector<vec3> & vertices, vector<vec3> & normals);
	virtual ~BufferedMesh();
	void bindVerticesBuffers();
	void bindNormalBuffers();
	GLuint getVerticesBuffer();
	GLuint getNormalBuffer();
	unsigned int getNumberOfVertices();
private:
	GLuint verticesBuffer;
	GLuint normalsBuffer;

	unsigned int numberOfVertices;

	void buffer(vector<vec3> & vertices, vector<vec3> & normals);
};

#endif /* BUFFEREDMESH_H_ */
