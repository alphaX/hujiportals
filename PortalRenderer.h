/*
 * PortalRenderer.h
 *
 *  Created on: Jun 1, 2013
 *      Author: alex
 */

#ifndef PORTALRENDERER_H_
#define PORTALRENDERER_H_

#include <GL/glew.h>
#include <GL/glfw.h>
#include <glm/glm.hpp>
#include <Cg/cgGL.h>

#include "GLProgram.h"

#include "Portal.h"
#include "PortalRenderer.h"
#include "PortalCamera.h"
#include "Scene.h"
#include "PhongRenderer.h"
#include "PortalGun.h"

class PortalRenderer: public GLRenderer {
public:
	PortalRenderer(PortalGun * portalGun, Camera * camera, Scene * scene, GLProgram * program);
	virtual ~PortalRenderer();

	void render(Entity & entity);
	void render();
private:
	PortalGun * portalGun;
	Camera * camera;
	Scene * scene;
	GLRenderer * portalShapeRenderer;
	GLProgram * program;
	void renderInStencil(Portal * portal);
	void renderInDepth(Portal * portal);
	void renderSceneThroughPortal(Portal * target, Portal * other);
	void renderSinglePortal(Portal * portal);
	void renderBothPortals();
};

#endif /* PORTALRENDERER_H_ */
