/*
 * ShaderProxy.cpp
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#include "ShaderProxy.h"

ShaderProxy::ShaderProxy(CGcontext cgContext,const char * fxFilepath) {
	this->cgContext = cgContext;
	this->fxFilepath = fxFilepath;
	cgVprofile = cgGLGetLatestProfile(CG_GL_VERTEX);
	cgFprofile = cgGLGetLatestProfile(CG_GL_FRAGMENT);
	cgGprofile = cgGLGetLatestProfile(CG_GL_GEOMETRY);
}

ShaderProxy::~ShaderProxy() {
	// TODO Auto-generated destructor stub
}

CGparameter ShaderProxy::getParameter(CGprogram & program, const char* name) {
	CGparameter parameter = cgGetNamedParameter(program, name);
	if(parameter == NULL){
		printf("%s parameter of file %s is invalid ", name, fxFilepath);
		exit(0);
	}
	return parameter;
}

CGprogram ShaderProxy::getVertexProgram(const char* programName) {
	return getProgram(programName, cgVprofile);
}

CGprogram ShaderProxy::getFragmentProgram(const  char* programName) {
	return getProgram(programName, cgFprofile);
}

CGprogram ShaderProxy::getGeometryProgram(const char* programName) {
	return getProgram(programName, cgGprofile);
}

CGprogram ShaderProxy::getProgram(const char* programName, CGprofile profile) {
	CGprogram cgProgram = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, profile, programName, 0);
	if(cgProgram == NULL)
		printf("program %s is not defined in file %s", programName, fxFilepath);

	cgGLLoadProgram(cgProgram);

	return cgProgram;
}
