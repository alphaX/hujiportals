/*
 * Clock.h
 *
 *  Created on: May 4, 2013
 *      Author: alex
 */

#ifndef CLOCK_H_
#define CLOCK_H_

#include <GL/glfw.h>

class Clock {
public:
	Clock();
	void update();
	double getTime();
	double getDt();
	virtual ~Clock();
private:
	double currentMessurment;
	double lastMessurment;
};

#endif /* CLOCK_H_ */
