/*
 * Camera.h
 *
 *  Created on: Jun 1, 2013
 *      Author: alex
 */

#ifndef CAMERA_H_
#define CAMERA_H_
#include <glm/glm.hpp>

using namespace glm;

class Camera {
public:
	Camera();
	virtual ~Camera();
	virtual mat4 getViewMatrix() = 0;
};

#endif /* CAMERA_H_ */
