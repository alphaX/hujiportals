/*
 * Portal.cpp
 *
 *  Created on: Jun 1, 2013
 *      Author: alex
 */

#include "Portal.h"

// portal - location and shape of portal
// portal renderer - takes 2 portals, creates cameras for them

Portal::Portal(vec3 location, vec2 dimension, vec3 normal) {
	this->location = location;
	this->dimension = dimension;
	this->normal = normal;

	material.color = vec3(0,0,0);
	material.kDiffuse = 0;

	createPortalMesh();
}

Portal::~Portal() {
	// TODO Auto-generated destructor stub
}

mat4 Portal::getWorldMatrix() {
	mat4 scaleM = scale(mat4(), vec3(dimension.x, dimension.y, 1));
	mat4 translateM = translate(mat4(), location);
	vec3 up = vec3(0,0,-1);

	mat4  rotateM(1);

	if(all(equal(normal, -up)))
		rotateM = rotate(180.0f, vec3(0,1,0));
	else
		rotateM = orientation(normalize(normal),up);

	return translateM*rotateM*scaleM;
}

vec3 Portal::getLocation() {
	return location;
}

void Portal::setLocation(vec3 location) {
	this->location = location;
}

bool Portal::intersectsWithLine(vec3 start, vec3 end, vec3& position, vec3 & normal) {
	vec3 vert0 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(1, 1, 0,1));
	vec3 vert1 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(-1, 1, 0,1));
	vec3 vert2 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(-1, -1, 0,1));
	vec3 vert3 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(1, -1, 0,1));

	normal = this->normal;
	return Intersections::rectWithLine(vert0, vert1, vert2, vert3, start, end, position);
}

bool Portal::intersectsWithRay(vec3 orig, vec3 dir, vec3 & position,vec3 & normal) {
	vec3 vert0 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(1, 1, 0,1));
	vec3 vert1 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(1, -1, 0,1));
	vec3 vert2 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(-1, 1, 0,1));
	vec3 vert3 = swizzle<X, Y, Z>(getWorldMatrix() * vec4(-1, -1, 0,1));

	normal = this->normal;
	return Intersections::rectWithRay(vert0, vert1, vert2, vert3, orig, dir, position);
}

void Portal::setNormal(vec3 normal) {
	this->normal = normal;
}

vec3 Portal::getNormal() {
	return normal;
}

vec2 Portal::getDimension() {
	return dimension;
}

void Portal::createPortalMesh() {
	vector<vec3> vetrices;
	vector<vec3> normals;

	vetrices.push_back(vec3(1.0f, -1.0f, 0.0f));
	vetrices.push_back(vec3(-1.0f, -1.0f, 0.0f));
	vetrices.push_back(vec3(1.0f, 1.0f, 0.0f));

	vetrices.push_back(vec3(1.0f, 1.0f, 0.0f));
	vetrices.push_back(vec3(-1.0f, -1.0f, 0.0f));
	vetrices.push_back(vec3(-1.0f, 1.0f, 0.0f));


	for (int i = 0; i < 6; i++)
		normals.push_back(vec3(0, 0, -1));

	mesh = new BufferedMesh(vetrices, normals);
}
