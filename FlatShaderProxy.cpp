/*
 * flatShaderProxy.cpp
 *
 *  Created on: Jun 21, 2013
 *      Author: alex
 */

#include "FlatShaderProxy.h"

FlatShaderProxy::FlatShaderProxy(CGcontext cgContext, const char* fxFilepath):ShaderProxy(cgContext, fxFilepath){
	// TODO Auto-generated constructor stub
	vertexShader = getVertexProgram("vs");
	fragmentShader = getFragmentProgram("fs");

	shaderPosition = getParameter(vertexShader, "pos");
}


FlatShaderProxy::~FlatShaderProxy() {
	// TODO Auto-generated destructor stub
}

void FlatShaderProxy::setPosition(GLuint verticesBuffer) {
	glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer);
	cgGLEnableClientState(shaderPosition);
	cgGLSetParameterPointer(shaderPosition, 3, GL_FLOAT, 0, 0);
}

void FlatShaderProxy::enable() {
	cgGLBindProgram(vertexShader);
	cgGLEnableProfile(cgVprofile);

	cgGLBindProgram(fragmentShader);
	cgGLEnableProfile(cgFprofile);
}

void FlatShaderProxy::clean() {
	cgGLDisableClientState(shaderPosition);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgFprofile);
}
