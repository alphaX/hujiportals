/*
 * CurserRenderer.cpp
 *
 *  Created on: Jun 21, 2013
 *      Author: alex
 */

#include "CurserRenderer.h"

CurserRenderer::CurserRenderer(GLProgram* program) {
	shaderProxy = new FlatShaderProxy(program->getContext(), "flatShader.cgfx");

	vector<vec3> vertices, normals;
	float d = 0.01;
	vertices.push_back(vec3(0,0,0));
	vertices.push_back(vec3(d,0,0));
	vertices.push_back(vec3(-d,0,0));
	vertices.push_back(vec3(0,d,0));
	vertices.push_back(vec3(0,-d,0));


	normals.push_back(vec3(0,1,0));


	curser = new BufferedMesh(vertices, normals);
}

CurserRenderer::~CurserRenderer() {
	// TODO Auto-generated destructor stub
}

void CurserRenderer::render() {
	glPointSize(0.5);
	shaderProxy->enable();
	shaderProxy->setPosition(curser->getVerticesBuffer());
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawArrays(GL_POINTS, 0, curser->getNumberOfVertices());
	shaderProxy->clean();
}
