/*
 * Cube.h
 *
 *  Created on: Jun 1, 2013
 *      Author: alex
 */

#ifndef CUBE_H_
#define CUBE_H_
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/swizzle.hpp>

#include "Entity.h"
#include "Material.h"
#include "Intersections.h"

using namespace glm;

class Cube: public Entity {
public:
	Cube(vec3 location,vec3 dimension, Material material, bool outer);
	virtual ~Cube();

	mat4 getWorldMatrix();
	bool intersectsWithLine(vec3 start, vec3 end, vec3 & position, vec3 & normal);
	bool intersectsWithRay(vec3 origin, vec3 dir, vec3 & position, vec3 & normal);
private:
	vec3 location;
	vec3 dimension;
	bool outer;
	void createCubeMesh(bool outer);
};

#endif /* CUBE_H_ */
