/*
 * PortalCamera.cpp
 *
 *  Created on: Jun 1, 2013
 *      Author: alex
 */

#include "PortalCamera.h"


PortalCamera::PortalCamera(Portal * srcPortal, Portal * dstPortal,
		Camera * mainCamera): srcPortal(srcPortal), dstPortal(dstPortal), mainCamera(mainCamera) {
}

PortalCamera::~PortalCamera() {
	// TODO Auto-generated destructor stub
}

mat4 PortalCamera::getViewMatrix() {
	glm::mat4 mv = mainCamera->getViewMatrix() * srcPortal->getWorldMatrix();
	//glm::mat4 mv = srcPortal.getWorldMatrix();
	glm::mat4 portalView =
		// 3. transformation from source portal to the camera - it's the
		//    first portal's ModelView matrix:
		mv
		// 2. object is front-facing, the camera is facing the other way:
		* glm::rotate(glm::mat4(1.0), 180.0f, glm::vec3(0.0,1.0,0.0))
		// 1. go the destination portal; using inverse, because camera
		//    transformations are reversed compared to object
		//    transformations:
		* glm::inverse(dstPortal->getWorldMatrix())
		;

	return portalView;
}
