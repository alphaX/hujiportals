/*
 * player.hpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */
#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/swizzle.hpp>

#include "PortalGun.h"
#include "Scene.h"

#include "fmod/inc/fmod.hpp"
#include "fmod/inc/fmod_errors.h"

using namespace glm;

const float GRAVITY_FORCE = 0.5;
const float EPSILON = 0.001;
const float OFFSET_FROM_SOLID = 0.13;
enum Direction {
	FORWARD,
	BACKWARDS,
	LEFT,
	RIGHT,
	UP,
	DOWN
} ;

typedef enum{
	NO_TURN,
	CLOCKWISE,
	ANTI_CLOCKWISE
} TurnDirection;

struct PlayerPropeties{
	float speed;
	float angleSpeed;
	float height;
	float jumpSpeed;
};

class Player{
public:
	Player(PlayerPropeties properties, PortalGun * portalGun, Scene * scene);
	void advance(Direction direction);
	void turn(float turnX, float turnY);
	void step(float dt);
	glm::vec3 getPosition();
	void setPosition(glm::vec3 position);
	glm::vec3 getCurrentTurn();
	void setTurnDirection(glm::vec3 turn);
	float getHeight();
	void firePortal(PortalIndex portalIndex);
	void collideWithScene(vec3 startPosition, vec3 endPosition);
private:
	Direction direction;
	glm::vec3 turnDirection;
	glm::vec3 position;
	glm::vec3 currentSelfForce;
	glm::vec3 currentSpeed;
	PlayerPropeties properties;
	PortalGun * portalGun;
	Scene * scene;
	bool onFloor;

	FMOD::System     *fmodSystem;
	FMOD::Sound      *warpSound;
	FMOD::Channel    *channel;

	bool findIntersectingPortal(vec3 startPosition, vec3 endPosition, Portal * & intersectingPortal, Portal * & otherPortal, vec3 & intersectionPoint);
	bool wrapThroughPortals(vec3 startPosition, vec3 endPosition);
	void updateOnFloor();
	mat4 createWarpingMatrix(Portal * srcPortal, Portal * dstPortal);
};


#endif /* PLAYER_HPP_ */
