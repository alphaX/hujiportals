/*
 * Controles.cpp
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#include "Controles.h"


Controles::Controles(Player & player): player(player) {
	// TODO Auto-generated constructor stub

}

Controles::~Controles() {
	// TODO Auto-generated destructor stub
}

void Controles::handleInput(){
	handleKeyboardInput();
	handleMouseInput();
}

void Controles::handleKeyboardInput(){
	if(glfwGetKey( 'W' ) == GLFW_PRESS)
		player.advance(FORWARD);

	if(glfwGetKey( 'S' ) == GLFW_PRESS)
		player.advance(BACKWARDS);

	if(glfwGetKey( 'A' ) == GLFW_PRESS)
		player.advance(LEFT);

	if(glfwGetKey( 'D' ) == GLFW_PRESS)
		player.advance(RIGHT);

	if(glfwGetKey( ' ' ) == GLFW_PRESS)
		player.advance(UP);

	if(glfwGetKey( 'C' ) == GLFW_PRESS)
		player.advance(DOWN);
}

void Controles::handleMouseInput(){
	int x,y;

	glfwGetMousePos(&x, &y);

	if(!firstFrame){
		float angleX = (float)(previouseMouseX - x);
		float angleY = (float)(previouseMouseY - y);
		player.turn(angleX, angleY);
	}

	previouseMouseX = x;
	previouseMouseY = y;
	firstFrame = false;

	if(glfwGetMouseButton( GLFW_MOUSE_BUTTON_1 ) == GLFW_PRESS){
		if(!orangePortalFired) {
			player.firePortal(ORANGE);
			orangePortalFired = true;
		}
	} else{
		orangePortalFired = false;
	}

	if(glfwGetMouseButton( GLFW_MOUSE_BUTTON_2 ) == GLFW_PRESS){
		if(!bluePortalFired){
			player.firePortal(BLUE);
			bluePortalFired = true;
		}
	} else{
		bluePortalFired = false;
	}


}
