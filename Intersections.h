/*
 * Intersections.h
 *
 *  Created on: Jun 8, 2013
 *      Author: alex
 */

#ifndef INTERSECTIONS_H_
#define INTERSECTIONS_H_
#include <glm/glm.hpp>
#include <glm/gtx/intersect.hpp>
#include <glm/gtc/swizzle.hpp>

using namespace glm;

class Intersections {
public:
	static bool rectWithRay(
			vec3 v1,vec3 v2,vec3 v3,vec3 v4,
			vec3 origin, vec3 dir, vec3 & position);

	static bool rectWithLine(
			vec3 v1,vec3 v2,vec3 v3,vec3 v4,
			vec3 start, vec3 end, vec3 & position);
};

#endif /* INTERSECTIONS_H_ */
