/*
 * Mesh.cpp
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#include "Mesh.h"

Mesh::Mesh(std::vector<vec3> & vertices, std::vector<uvec3> & indexes, std::vector<vec3> & normals):
vertices(vertices), indexes(indexes), normals(normals)
{}

std::vector<vec3> & Mesh::getVertices(){
	return vertices;
}
std::vector<uvec3> & Mesh::getIndexes(){
	return indexes;
}

int Mesh::getIndexesNumber(){
	return indexes.size();
}

Mesh::~Mesh() {
	// TODO Auto-generated destructor stub
}

