/*
 * WallRenderer.cpp
 *
 *  Created on: May 31, 2013
 *      Author: alex
 */

#include "PhongRenderer.h"

PhongRenderer::PhongRenderer(CGcontext cgContext)
{
	camera = NULL;
	shaderProxy = new PhongShaderProxy(cgContext, "cgPhong.cgfx");
}

PhongRenderer::~PhongRenderer() {
	// TODO Auto-generated destructor stub
}

void PhongRenderer::render(Entity & entity) {
//	glBindBuffer(GL_ARRAY_BUFFER, entity.getVerticesBuffer());
	shaderProxy->enable();
	mat4 w = entity.getWorldMatrix();

	mat4 v = camera->getViewMatrix();

	mat4 p = projection;

	shaderProxy->setWvp(w, v, p);
	shaderProxy->setPosition(entity.getVerticesBuffer());
	shaderProxy->setNormals(entity.getNormalBuffer());
	shaderProxy->setColor(entity.getMaterial().color);
	shaderProxy->setKd(entity.getMaterial().kDiffuse);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawArrays(GL_TRIANGLES, 0, entity.getNumberOfVertices());
	shaderProxy->clean();
}
