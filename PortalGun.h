/*
 * PortalGun.h
 *
 *  Created on: Jun 8, 2013
 *      Author: alex
 */

#ifndef PORTALGUN_H_
#define PORTALGUN_H_

#include <glm/glm.hpp>
#include <vector>

#include "Portal.h"
#include "Scene.h"

#include "fmod/inc/fmod.hpp"
#include "fmod/inc/fmod_errors.h"

using namespace glm;

typedef enum {
	ORANGE,
	BLUE
} PortalIndex;
class PortalGun {
public:
	PortalGun(Scene * world);
	virtual ~PortalGun();

	void fireAt(vec3 fromPosition, vec3 direction, PortalIndex portal);
	Portal * getPortal(PortalIndex portal);
	bool portalExists(PortalIndex portal);
private:
	Portal * orangePortal;
	Portal * bluePortal;
	Scene * world;

	vec2 defaultPortalDimension;
	FMOD::System     *fmodSystem;
	FMOD::Sound      *portalSound, *warpSound;
	FMOD::Channel    *channel;
	void createPortal(PortalIndex portal, vec3 location, vec3 normal);
};

#endif /* PORTALGUN_H_ */
